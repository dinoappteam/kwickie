package kwickie.hosameldin.com.kwickie.activity;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import kwickie.hosameldin.com.kwickie.Adapter.FeedAdapter;
import kwickie.hosameldin.com.kwickie.R;
import kwickie.hosameldin.com.kwickie.listener.EndlessRecyclerOnScrollListener;

import kwickie.hosameldin.com.kwickie.http.httpRequest;
import kwickie.hosameldin.com.kwickie.listener.OnHttpRequestCompleted;
import kwickie.hosameldin.com.kwickie.model.ApprovedQuestion;
import kwickie.hosameldin.com.kwickie.parse.JsonParser;
import kwickie.hosameldin.com.kwickie.util.Constant;

/**
* The login has been hardcoded.
*
 * Activity cycle:
 * 1-automated login
 * 2-calling approved API (Feed)
 * 3-adding result in a private shared preference
 * 4-load feed items 10 by 10
*
* */
public class MainActivity extends AppCompatActivity implements OnHttpRequestCompleted{
    private List<ApprovedQuestion> approvedQuestionList;
    private RecyclerView recyclerView;
    private TextView tvLoadMore, tvLoading;
    private LinearLayout llLoading;
    private String EMAIL = "shane.heal@kwickie.com";
    private String PASSWORD = "password";
    private FeedAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private int offset=0,limit=10;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       initViews();
       login();
    }

    @Override
    public void onSuccess(int Code, String response) {
        Log.i("response code","code: "+Code);
        if(Code == Constant.CODE_FEED)
        {
            tvLoading.setText(getResources().getString(R.string.tv_status_wrapping_up));

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(Constant.sharedPrefName,response);
            editor.apply();

            approvedQuestionList = JsonParser.parseApprovedPosts(this,offset,limit);
            adapter = new FeedAdapter(this, approvedQuestionList);
            recyclerView.setAdapter(adapter);
           updateUI(View.GONE);
        }
        else if(Code == Constant.CODE_LOGIN)
        {
            tvLoading.setText(getResources().getString(R.string.tv_status_feed));
            loadFeed(response);

        }
    }
    private void initViews(){
        recyclerView = (RecyclerView)findViewById(R.id.recycleView);
        llLoading = (LinearLayout)findViewById(R.id.ll_loading);
        tvLoading = (TextView)findViewById(R.id.tv_loading);
        tvLoadMore = (TextView)findViewById(R.id.tv_load_more);
        tvLoadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //hide load more button
                v.setVisibility(View.GONE);
                //checking the next 10 items
                offset+=10;
                limit+=10;
                List<ApprovedQuestion> approvedQuestionsLoadMoreList = new ArrayList<ApprovedQuestion>();
                approvedQuestionsLoadMoreList = JsonParser.parseApprovedPosts(MainActivity.this,offset,limit);
                approvedQuestionList.addAll(approvedQuestionsLoadMoreList);

                int curSize = adapter.getItemCount();
               // recyclerView.setAdapter(adapter);
                adapter.notifyItemRangeInserted(curSize, approvedQuestionList.size() - 1);
                //adapter.notifyDataSetChanged();
                //adapter = new FeedAdapter(MainActivity.this, approvedQuestionList);
                //recyclerView.setAdapter(adapter);
            }
        });
         linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        //check if scrolled till the end of screen to show load more textview
        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {

                tvLoadMore.setVisibility(View.VISIBLE);
            }
        });
    }
    //login using harcoded credentials
    private  void login(){

        tvLoading.setText(getResources().getString(R.string.tv_status_login));
        JSONObject jsonBodyObj = new JSONObject();
        try{
            jsonBodyObj.put("email", EMAIL);
            jsonBodyObj.put("password",PASSWORD);
        }catch (JSONException e){
            e.printStackTrace();
        }
        new httpRequest(this,jsonBodyObj,Constant.CODE_LOGIN,this);
    }
    //calling approved API
    private void loadFeed(String response){

        JSONObject jo = null;
        try {
            jo = new JSONObject(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(JsonParser.getAccessToken(jo))
        {
            new httpRequest(this,Constant.CODE_FEED,this);
        }
    }
    //show or hide loading linear layout
    private void updateUI(int visibility)
    {
        llLoading.setVisibility(visibility);
    }
}
