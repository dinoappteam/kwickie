package kwickie.hosameldin.com.kwickie.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import kwickie.hosameldin.com.kwickie.R;
import kwickie.hosameldin.com.kwickie.activity.VideoViewActivity;
import kwickie.hosameldin.com.kwickie.model.ApprovedQuestion;
import kwickie.hosameldin.com.kwickie.util.Constant;

/**
 * Created by hossam on 6/1/16.
 */
public class FeedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context context;
    private int itemsCount = 10;
    private List<ApprovedQuestion> items;
    public FeedAdapter(Context context, List<ApprovedQuestion> items) {
        this.context = context;
        this.items = items;
        itemsCount = items.size();
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(context).inflate(R.layout.item_feed, parent, false);
        return new RowHomeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        final RowHomeViewHolder holder = (RowHomeViewHolder) viewHolder;
        String name = "<b>"+items.get(position).getUserQuestion().getFullName()+"</b> had a kwickie with <b>"+items.get(position).getUserAnswer().getFullName()+"</b>";
        holder.tvNames.setText(Html.fromHtml(name));
        Uri uriUser = Uri.parse(items.get(position).getUserQuestion().getProfilePicturePath());
        holder.ivUser.setImageURI(uriUser);
        Uri uriThumbnail = Uri.parse(items.get(position).getKwickieVideo().getThumbUrl());

        holder.ivThumbnail.setImageURI(uriThumbnail);
        holder.ivThumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, VideoViewActivity.class);
                intent.putExtra(Constant.INTENT_VIDEOURL,items.get(position).getKwickieVideo().getLowQualityUrl());
                context.startActivity(intent);
                ((AppCompatActivity)context).overridePendingTransition(R.anim.right_to_left, R.anim.no_anim);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
    public static class RowHomeViewHolder extends RecyclerView.ViewHolder {
        TextView tvNames;
        SimpleDraweeView ivUser,ivThumbnail;

        public RowHomeViewHolder(View view) {
            super(view);
            tvNames = (TextView) view.findViewById(R.id.tv_names_feed);
            ivUser = (SimpleDraweeView) view.findViewById(R.id.iv_user_feed);
            ivThumbnail = (SimpleDraweeView) view.findViewById(R.id.iv_thumbnail_feed);

        }
    }
}