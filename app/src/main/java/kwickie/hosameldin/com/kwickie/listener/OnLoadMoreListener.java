package kwickie.hosameldin.com.kwickie.listener;

/**
 * Created by hossam on 6/1/16.
 */
public interface OnLoadMoreListener {
    void onLoadMore();
}
