package kwickie.hosameldin.com.kwickie.util;

/**
 * Created by hossam on 6/1/16.
 */
public class Constant {
    public static String TAG = "Kwickie";
    public static String accessToken ="";
    public static String host="https://bigdev.kwickie.com/api/kwickies/approved?access_token=";
    public static String sharedPrefName = "com.kwickie.hosameldin";
    public static String login = "https://bigdev.kwickie.com/api/members/login";
    public static int CODE_LOGIN = 11;
    public static int CODE_FEED = 100;
    public static String INTENT_VIDEOURL = "video_url";
}
