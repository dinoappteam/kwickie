package kwickie.hosameldin.com.kwickie.model;

/**
 * Created by hossam on 6/1/16.
 */
public class ApprovedQuestion {
   KwickieVideo kwickieVideo;
    User userQuestion,userAnswer;

    public ApprovedQuestion() {
    }

    public ApprovedQuestion(KwickieVideo kwickieVideo, User userQuestion, User userAnswer) {
        this.kwickieVideo = kwickieVideo;
        this.userQuestion = userQuestion;
        this.userAnswer = userAnswer;
    }

    public KwickieVideo getKwickieVideo() {
        return kwickieVideo;
    }

    public void setKwickieVideo(KwickieVideo kwickieVideo) {
        this.kwickieVideo = kwickieVideo;
    }

    public User getUserQuestion() {
        return userQuestion;
    }

    public void setUserQuestion(User userQuestion) {
        this.userQuestion = userQuestion;
    }

    public User getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(User userAnswer) {
        this.userAnswer = userAnswer;
    }
}
