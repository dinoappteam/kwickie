package kwickie.hosameldin.com.kwickie.http;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import kwickie.hosameldin.com.kwickie.listener.OnHttpRequestCompleted;
import kwickie.hosameldin.com.kwickie.util.Constant;

/**
 * Created by hossam on 5/10/16.
 */
public class httpRequest {
    Context context;
    OnHttpRequestCompleted listener;
    int code;
    public httpRequest(Context context, JSONObject jsonBodyObj, int code, OnHttpRequestCompleted listener) {
        this.context = context;
       // httpVolleyRequest(Request.Method.GET);
      //  httpVolleyPost();
        this.listener = listener;
        this.code = code;
        httpPost( jsonBodyObj);


    }
    public httpRequest(Context context, int code, OnHttpRequestCompleted listener) {
        this.context = context;
        // httpVolleyRequest(Request.Method.GET);
        //  httpVolleyPost();
        this.listener = listener;
        this.code = code;
        httpGet();


    }

    private void httpPost(JSONObject jsonBodyObj){
        String url = Constant.login;


        final String requestBody = jsonBodyObj.toString();

        JsonObjectRequest JOPR = new JsonObjectRequest(Request.Method.POST,
                url, null, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {
                try {
                    listener.onSuccess(code,response.toString());
                    VolleyLog.v("Response:%n %s", response.toString(4));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(error.networkResponse!=null) {
                    VolleyLog.e("Error: ", error.networkResponse.statusCode);
                    int statusCode = error.networkResponse.statusCode;
                    NetworkResponse response = error.networkResponse;

                    Log.d("testerror", "" + statusCode + " " + response.data);
                    if (error instanceof NetworkError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof AuthFailureError) {
                        Log.d("testerror", "UnAuthorized");
                    } else if (error instanceof ParseError) {
                    } else if (error instanceof NoConnectionError) {
                    } else if (error instanceof TimeoutError) {
                    }
                }

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }


            @Override
            public byte[] getBody() {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                            requestBody, "utf-8");
                    return null;
                }
            }


        };
        Volley.newRequestQueue(context).add(JOPR);
    }

    private void httpGet(){
        String url = Constant.host;
     /*   Ion.with(context)
                .load(url+Constant.accessToken)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        // do stuff with the result or error
                        Log.i("exception",e.toString());
                        try {
                            listener.onSuccess(code,new JSONObject(result.toString()));
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }
                });*/

        //Log.i("url",url+Constant.accessToken+"&offset=0&limit=1");

        StringRequest getRequest = new StringRequest(Request.Method.GET, url+Constant.accessToken,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // display response
                Log.i(Constant.TAG,"done");


                            listener.onSuccess(code, response);


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(error.networkResponse!=null){
                        VolleyLog.e("Error: ", error.networkResponse.statusCode);
                        int  statusCode = error.networkResponse.statusCode;
                        NetworkResponse response = error.networkResponse;

                        Log.d("testerror", "" + statusCode + " " + response.data);
                        if( error instanceof NetworkError) {
                        } else if( error instanceof ServerError) {
                        } else if( error instanceof AuthFailureError) {
                            Log.d("testerror","UnAuthorized");
                        } else if( error instanceof ParseError) {
                        } else if( error instanceof NoConnectionError) {
                        } else if( error instanceof TimeoutError) {
                        }
                    }
                        else{
                            Log.i(Constant.TAG,"unexpected error");
                            error.printStackTrace();
                            //unexpected error
                        }
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");

                return headers;
            }
        };
        getRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(context).add(getRequest);
    }
}
