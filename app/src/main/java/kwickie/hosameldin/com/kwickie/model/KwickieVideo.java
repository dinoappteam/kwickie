package kwickie.hosameldin.com.kwickie.model;

/**
 * Created by hossam on 6/1/16.
 */
public class KwickieVideo {
    /*
    *
    * "kwickieVideo":{"processId":"18882",
    * "name":null,
    * "accountId":0,
    * "complete":1,
    * "still":0,
    * "stillSeconds":0,
    * "description":null,
    * "duration":21826,"createdAt":"2016-06-01T04:01:34.000Z",
    * "lowQualityUrl":"http://d12b7ev0vur5ia.cloudfront.net/7cf9d218882cd8a33ee083b65dd2a75379c2fc673d442b25c6c4da87bca105_low.mp4",
    * "highQualityUrl":"http://d12b7ev0vur5ia.cloudfront.net/7cf9d218882cd8a33ee083b65dd2a75379c2fc673d442b25c6c4da87bca105.mp4",
    * "playlistUrl":"http://d12b7ev0vur5ia.cloudfront.net/7cf9d218882cd8a33ee083b65dd2a75379c2fc673d442b25c6c4da87bca105_part.m3u8",
    * "processPlaylistUrl":"http://d12b7ev0vur5ia.cloudfront.net/7cf9d218882cd8a33ee083b65dd2a75379c2fc673d442b25c6c4da87bca105_part.m3u8",
    * "thumbWidth":640,
    * "thumbHeight":360,
    * "thumbUrl":"http://d12b7ev0vur5ia.cloudfront.net/7cf9d218882cd8a33ee083b65dd2a75379c2fc673d442b25c6c4da87bca105_circle_thumb.jpg",
    * "posterUrl":"http://d12b7ev0vur5ia.cloudfront.net/7cf9d218882cd8a33ee083b65dd2a75379c2fc673d442b25c6c4da87bca105_share.png",
    * "alertThumbUrl":null,
    * "alertThumbWidth":0,
    * "alertThumbHeight":0,
    * "id":18882}
    * */
    String processId,name,description, strCreatedAt,lowQualityUrl,highQualityUrl,playlistUrl,processPlaylistUrl,thumbUrl,posterUrl;
    int id,accountId,complete,still,stillSeconds,thumbHeight,thumbWidth,alertThumbHeight,alertThumbWidth;

    public KwickieVideo() {
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStrCreatedAt() {
        return strCreatedAt;
    }

    public void setStrCreatedAt(String strCreatedAt) {
        this.strCreatedAt = strCreatedAt;
    }

    public String getLowQualityUrl() {
        return lowQualityUrl;
    }

    public void setLowQualityUrl(String lowQualityUrl) {
        this.lowQualityUrl = lowQualityUrl;
    }

    public String getHighQualityUrl() {
        return highQualityUrl;
    }

    public void setHighQualityUrl(String highQualityUrl) {
        this.highQualityUrl = highQualityUrl;
    }

    public String getPlaylistUrl() {
        return playlistUrl;
    }

    public void setPlaylistUrl(String playlistUrl) {
        this.playlistUrl = playlistUrl;
    }

    public String getProcessPlaylistUrl() {
        return processPlaylistUrl;
    }

    public void setProcessPlaylistUrl(String processPlaylistUrl) {
        this.processPlaylistUrl = processPlaylistUrl;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getComplete() {
        return complete;
    }

    public void setComplete(int complete) {
        this.complete = complete;
    }

    public int getStill() {
        return still;
    }

    public void setStill(int still) {
        this.still = still;
    }

    public int getStillSeconds() {
        return stillSeconds;
    }

    public void setStillSeconds(int stillSeconds) {
        this.stillSeconds = stillSeconds;
    }

    public int getThumbHeight() {
        return thumbHeight;
    }

    public void setThumbHeight(int thumbHeight) {
        this.thumbHeight = thumbHeight;
    }

    public int getThumbWidth() {
        return thumbWidth;
    }

    public void setThumbWidth(int thumbWidth) {
        this.thumbWidth = thumbWidth;
    }

    public int getAlertThumbHeight() {
        return alertThumbHeight;
    }

    public void setAlertThumbHeight(int alertThumbHeight) {
        this.alertThumbHeight = alertThumbHeight;
    }

    public int getAlertThumbWidth() {
        return alertThumbWidth;
    }

    public void setAlertThumbWidth(int alertThumbWidth) {
        this.alertThumbWidth = alertThumbWidth;
    }
}
